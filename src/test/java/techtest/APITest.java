package techtest;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.response.ResponseBody;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import static io.restassured.RestAssured.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class APITest {
    public Gson gson = new Gson();

    @Before
    public void setup() {
        RestAssured.basePath = System.getProperty("basePath");
        System.out.println(basePath);
        given().when().get(basePath).then().statusCode(200);
    }

    @Test
    @Order(0)
    public void check_instruction_endpoint(){
       get(basePath + "instructions").then()
           .assertThat()
           .statusCode(200);
    }

    @Test
    @Order(1)
    public void check_users_endpoint(){
        get(basePath + "users").then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    @Order(3)
    public void check_user_endpoint()
    {
        String randomUserID = Actions.get_random_user(basePath);
        get(basePath + "user/" + randomUserID).then()
                .assertThat()
                .statusCode(200);
    }


    @Test
    @Order(4)
    public void check_city_endpoint()
    {
        String randomCity = Actions.get_random_city(basePath);
        get(basePath + "city/" + randomCity + "/users").then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    @Order(5)
    public void check_invalid_user()
    {
        get(basePath + "user/aaa").then()
                .assertThat()
                .statusCode(404);
    }

    @Test
    @Order(6)
    public void assert_user_response()
    {
        ResponseBody userBody = get(basePath + "user/" + Actions.get_random_user(basePath)).getBody();
        String json = gson.toJson(userBody.asString());
        Assert.assertEquals(json.contains("id"), true);
        Assert.assertEquals(json.contains("first_name"), true);
        Assert.assertEquals(json.contains("last_name"), true);
        Assert.assertEquals(json.contains("email"), true);
        Assert.assertEquals(json.contains("ip_address"), true);
        Assert.assertEquals(json.contains("latitude"), true);
        Assert.assertEquals(json.contains("longitude"), true);
        Assert.assertEquals(json.contains("city"), true);
    }

    @Test
    @Order(7)
    public void assert_users_response()
    {
        ResponseBody userBody = get(basePath + "user/" + Actions.get_random_user(basePath)).getBody();
        String json = gson.toJson(userBody.asString());
        Assert.assertEquals(json.contains("id"), true);
        Assert.assertEquals(json.contains("first_name"), true);
        Assert.assertEquals(json.contains("last_name"), true);
        Assert.assertEquals(json.contains("email"), true);
        Assert.assertEquals(json.contains("ip_address"), true);
        Assert.assertEquals(json.contains("latitude"), true);
        Assert.assertEquals(json.contains("longitude"), true);
    }

    @Test
    @Order(8)
    public void assert_city_response()
    {
        ResponseBody userBody = get(basePath + "city/" + Actions.get_random_city(basePath) + "/users").getBody();
        String json = gson.toJson(userBody.asString());
        Assert.assertEquals(json.contains("id"), true);
        Assert.assertEquals(json.contains("first_name"), true);
        Assert.assertEquals(json.contains("last_name"), true);
        Assert.assertEquals(json.contains("email"), true);
        Assert.assertEquals(json.contains("ip_address"), true);
        Assert.assertEquals(json.contains("latitude"), true);
        Assert.assertEquals(json.contains("longitude"), true);
    }

    @Test
    @Order(9)
    public void assert_instructions_response()
    {
        ResponseBody instructionBody = get(basePath + "instructions").getBody();
        String json = gson.toJson(instructionBody.asString());
        System.out.println(json);
        Assert.assertEquals(json.contains("todo"), true);
    }
}
