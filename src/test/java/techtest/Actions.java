package techtest;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import java.util.ArrayList;
import java.util.Random;


public class Actions {
    private static ArrayList userList;
    private static Random randomGenerator = new Random();

    public static ArrayList get_user_list(String basePath) {
        RequestSpecification httpRequest = RestAssured.given();
        ResponseBody response = httpRequest.get(basePath + "users").getBody();
        JsonPath jsonPathEvaluator = response.jsonPath();
        return jsonPathEvaluator.get("id");
    }

    public static String get_random_user(String basePath) {
        userList  = Actions.get_user_list(basePath);
        int index = randomGenerator.nextInt(userList.size());
        return userList.get(index).toString();
    }

    public static String get_random_city(String basePath) {
        String randomUserId = get_random_user(basePath);
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get(basePath + "user/" + randomUserId);
        ResponseBody body = response.getBody();
        JsonPath jsonPathEvaluator = body.jsonPath();
        return jsonPathEvaluator.get("city");
    }

}
