Initial branch creation on new repo for BPDTS technical test.

Tests ran using JUnit. System property for the basePath must be set in either the Test Config, 
or as a command line argument when running the tests.

Command to run tests - "mvn -DargLine="-DbasePath=http://bpdts-test-app-v2.herokuapp.com/" test"

Initial build of gitlab pipeline has been created, 
tests are running in the pipeline but the api is refusing the connection.

If time allowed I would add additional tests to ensure the operations are pulling back the correct information.
i.e. user the 'users' endpoint to pull all users then do a count of users for a specific city, 
then use that city to retrieve the list of users specific to that location and check the counts match.